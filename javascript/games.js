
// param url: Url to torneopal teams page 
function get_games_fin(url) {
    $.ajax({
        url: url,
        context: document.body,
        dataType: "html",
        success: function(html) {
            // Add head to table
            $('#games').append(
            "<table class='table table-dark table-hover table-bordered custom-table' id='games_table'>" +
                "<thead class='custom-thead'>" +
                    "<tr>" +
                        "<th>Ottelu</th>" +
                        "<th>Viikonpäivä</th>" +
                        "<th>Päivämäärä</th>" +
                        "<th>Koti</th>" +
                        "<th>Vieras</th>" +
                        "<th>Info</th>" +
                    "</tr>" +
                "</thead>" +
                "<tbody id='games_body'>" +
                "</tbody>" +
            "</table>");

            // Get correct data from ajax
            var data = $(html).find('#olista').find('tr');

            // Add rows
            $('#games_body').html(data);

            // Remove first
            $('#games_body tr:first-child').remove();

            // Remove pdf column
            $('#games_body td.col_pdf').remove();
            
            // Change game links
            $('#games_body .col_ottelunro a').each(function() {
                $(this).attr("href", "https://lentopallo.torneopal.fi" + $(this).prop('pathname') + $(this).prop('search'));
                $(this).attr("target", "_blank");
            });

            // Change team links
            $('#games_body .col_kotisiisti a').each(function() {
                $(this).attr("href", "https://lentopallo.torneopal.fi" + $(this).prop('pathname') + $(this).prop('search'));
                $(this).attr("target", "_blank");
            });

            $('#games_body .col_vierassiisti a').each(function() {
                $(this).attr("href", "https://lentopallo.torneopal.fi" + $(this).prop('pathname') + $(this).prop('search'));
                $(this).attr("target", "_blank");
            });
        },

        error: function(xhr, textStatus, errorThrown) {
            //try again
            $(window).ready( function() {
                if (window.location.href.indexOf('reload') == -1) {
                    window.location.replace(window.location.href+'?reload');
                }
                else if (window.location.href.indexOf('reload') != -1) {
                    $('#main').append("<center><p>Pelien haku epäonnistui, lataa sivu uudelleen.</p></center>");
                }
            });
            return;
        }
    });
}

function get_serie_fin(url) {
    $.ajax({
        url: url,
        context: document.body,
        dataType: "html",
        tryCount : 0,
        retryLimit : 3,
        success: function(html) {

            // Get data
            var data = $(html).find('#joukkueet');

            // Append data
            $('#serie').append(data);

            // Remove events
            $('#serie table tbody tr').prop( "onclick", null );
            $('#serie table tbody tr').prop( "onmouseover", null );
            $('#serie table tbody tr').prop( "onmouseout", null );

            // Add class to table
            $('#serie table').addClass('table table-dark table-hover table-bordered custom-table');

            // Change team links
            $('#serie .lohko_joukkue a').each(function() {
                $(this).attr("href", "https://lentopallo.torneopal.fi" + $(this).prop('pathname') + $(this).prop('search'));
                $(this).attr("target", "_blank");
            });

            // Fix thead
            $('#serie table thead').replaceWith(
                "<thead class='custom-thead'>" +
                    "<tr>" +
                        "<th>Joukkue</th>" +
                        "<th>Pelit</th>" +
                        "<th>Erät</th>" +
                        "<th>Eräpisteet</th>" +
                        "<th>Pisteet</th>" +
                    "</tr>" +
                "</thead>");

            // Remove logo column
            $('.lohko_logo').remove();

            // Remove kunto column
            $('.lohko_kunto').remove();
        },

        error: function(xhr, textStatus, errorThrown) {
            //try again
            $(window).ready( function() {
                if (window.location.href.indexOf('reload') == -1) {
                    window.location.replace(window.location.href+'?reload');
                }
                else if (window.location.href.indexOf('reload') != -1) {
                    $('#main').append("<center><p>Sarjan haku epäonnistui, lataa sivu uudelleen.</p></center>");
                }
            });
            return;      
        }
    });
}