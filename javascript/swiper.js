$(document).ready(function () {

    var swiper = new Swiper('.swiper-container', {
        speed: 400,
        loop: false,

        // Navigation arrows
        navigation: {
            nextEl: '.swiper-button-next',
            prevEl: '.swiper-button-prev',
        },

        slidesPerView: 3,
        spaceBetween: 50,
        centeredSlides: false,
        effect: 'slide',

        pagination: {
            el: '.swiper-pagination',
            clickable: true,
            type: 'bullets'
        },

        grabCursor: true,

        breakpoints: {
            // when window width is <= 320px
            480: {
            slidesPerView: 1,
            spaceBetween: 10
            },
            // when window width is <= 480px
            640: {
            slidesPerView: 2,
            spaceBetween: 20
            }
        }
    });
});