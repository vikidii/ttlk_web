// Scrolling to top button functionality
$(document).ready(function () {
    "use strict";
    $(window).scroll(function () {
        setTimeout(function () {
            if ($(this).scrollTop() > 200) {
                $('.scrollToTop').fadeIn();
            } else {
                $('.scrollToTop').fadeOut();
            }
        }, 100);
    });
    // Smoothing
    $('.scrollToTop').click(function () {
        $('html, body').animate({scrollTop: 0}, 1000);
    });
});