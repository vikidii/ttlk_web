
TTLK (Tampereen Teekkareiden LentopalloKerho) website project.
Websites can be found from www.students.tut.fi/~ttlk/

Pages are being designed by Ville Saarinen, Juho Hulkkonen
and Teemu Pöytäniemi.

Mainly usign HTML5, CSS/Bootstrap and JavaScript.
Might start using php in future.

The server is owned by Tampere University.

Pages have been updated last in 2010, so this project is in the start.

